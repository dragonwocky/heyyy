/*
 * heyyy - Say hiii, say heyyy, say g'day!
 * ========================================
 *
 * ISC License
 *
 * Copyright (c) 2019, dragonwocky <thedragonring.bod@gmail.com> (https://dragonwocky.me/)
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

window.onload = () => {
  if (!WebSocket) {
    document.querySelector('.heyyy').innerHTML = `
      <div class="error">
        <p>
          Your browser does not support WebSockets! <br> Go to <a
            href="https://caniuse.com/#search=websockets"
            target="_blank"
          >https://caniuse.com/#search=websockets</a> to find out which browser versions support WebSockets.
        </p>
      </div>
    `;
  } else {
    // DATA
    const alert = new Audio('/ding.mp3'),
      doctitle = '' + document.title;
    let $settings = {},
      $options = {},
      uid = 0;
    const $defaults = {
        username: '',
        room: '',
        history: false,
        notifications: true
      },
      data = replace => {
        $settings = {
          ...$defaults,
          ...(localStorage.heyyy ? JSON.parse(localStorage.heyyy) : {}),
          ...(replace || {})
        };
        Object.keys($settings).forEach(key => {
          if (
            !Object.keys($defaults).includes(key) ||
            typeof $settings[key] !== typeof $defaults[key]
          )
            delete $settings[key];
        });
        localStorage.heyyy = JSON.stringify($settings);
      };
    if (window.location.search)
      window.location.search
        .slice(1)
        .split('&')
        .forEach(o => {
          $options[o.split('=')[0]] = o.split('=')[1]
            ? !isNaN(o.split('=')[1])
              ? parseInt(o.split('=')[1])
              : o.split('=')[1] === 'true'
              ? true
              : o.split('=')[1] === 'false'
              ? false
              : o.split('=')[1]
            : true;
        });
    data($options);

    // SETUP
    function setup() {
      document.querySelector('.heyyy').innerHTML = `
        <div class="set">
          <h1>heyyy! <img src="/wave.png"></img></h1>
          <br>
          <div class="columns">
            <div class="rows">
              <input name="username" placeholder="username" type="text" value="${
                $settings.username
              }">
              <input name="room" placeholder="room" type="text" value="${
                $settings.room
              }">
            </div>
            <button>join</button>
          </div>
          <br>
          <p class="details">
            <span class="spaces">usernames and room names cannot contain spaces</span>
            <br>
            <span class="required">a username is required</span>
          </p>
          <br>
          <div class="checks">
            <div class="check">
              <input id="check-history" type="checkbox" name="history" ${
                $settings.history ? 'checked' : ''
              }>
              <label for="check-history">show history</label>
            </div>
            <div class="check">
              <input id="check-notifs" name="notifications" type="checkbox" ${
                $settings.notifications ? 'checked' : ''
              }>
              <label for="check-notifs">notification sounds</label>
            </div>
          </div>
        </div>
      `;
      const $set = {
          affected: false,
          username: document.querySelector(
            '.heyyy .set input[name="username"]'
          ),
          room: document.querySelector('.heyyy .set input[name="room"]'),
          history: document.querySelector('.heyyy .set input[name="history"]'),
          notifications: document.querySelector(
            '.set input[name="notifications"]'
          ),
          spaces: document.querySelector('.heyyy .set span.spaces'),
          required: document.querySelector('.heyyy .set span.required'),
          submit: document.querySelector('.heyyy .set button')
        },
        checkUsername = () => {
          let valid = true;
          $set.username.className = '';
          $set.required.className = 'required';
          $set.spaces.classList.remove('userwarn');
          if ($set.username.value.trim().includes(' ')) {
            valid = false;
            $set.username.className = 'warn';
            $set.spaces.classList.add('userwarn');
          }
          if (!$set.username.value.trim()) {
            valid = false;
            if ($set.affected) {
              $set.username.className = 'warn';
              $set.required.className = 'required userwarn';
            }
          }
          data({ username: $set.username.value.trim() });
          return valid;
        },
        checkRoom = () => {
          let valid = true;
          $set.room.className = '';
          $set.spaces.classList.remove('roomwarn');
          if ($set.room.value.trim().includes(' ')) {
            valid = false;
            $set.room.className = 'warn';
            $set.spaces.classList.add('roomwarn');
          }
          data({ room: $set.room.value.trim() });
          return valid;
        };
      $set.username.onkeyup = e => {
        if (e.keyCode === 13) $set.room.focus();
      };
      $set.username.oninput = e => {
        $set.affected = true;
        checkUsername();
      };
      checkUsername();
      $set.room.onkeyup = e => {
        if (e.keyCode == 13) {
          if ($set.username.value.trim()) {
            run();
          } else $set.username.focus();
        }
      };
      $set.room.oninput = checkRoom;
      checkRoom();
      $set.history.onclick = () => {
        data({
          history: $set.history.checked
        });
      };
      $set.notifications.onclick = () => {
        data({
          notifications: $set.notifications.checked
        });
      };
      $set.submit.onclick = run;
      if ($options.autojoin) run();

      function run() {
        $set.affected = true;
        if (!checkUsername() || !checkRoom()) return undefined;
        chat();
      }
    }

    // CHAT
    function chat() {
      document.querySelector('.heyyy').innerHTML = `
        <div class="chat">
          <section class="main">
            <div class="messages">
              <div class="msg">
                <p class="title">
                  <span class="author">vines</span>
                  <span class="time">yesterday at 3:45 pm</span>
                </p>
                <p class="text">
                  how do you use a fork as a keyboard?
                </p>
              </div>
              <div class="msg">
                <p class="title">
                  <span class="author">skink</span>
                  <span class="time">yesterday at 3:47 pm</span>
                </p>
                <p class="text">
                  I slap it with my paws
                </p>
              </div>
            </div>
            <aside>
              <span class="status">OFFLINE</span>
              <div class="list">
              </div>
              <button>leave</button>
            </aside>
          </section>
          <div class="send">
            <textarea name="draft" placeholder="say ${
              ['hiii', 'heyyy', "g'day"][Math.floor(Math.random() * 3)]
            }..." rows="1"></textarea>
            <button>send</button>
          </div>
        </div>
      `;
      const $chat = {
        input: document.querySelector('.heyyy .chat .send textarea'),
        button: document.querySelector('.heyyy .chat .send button')
      };
      autosize($chat.input);

      connect();
    }

    // SOCKET
    function connect() {
      const wss = new WebSocket('ws://localhost:3000');
      wss.parse = content => {
        wss.send(JSON.stringify(content));
      };
      wss.onopen = () => {
        if ($settings.room) document.title = `${doctitle} - ${$settings.room}`;
        wss.parse({
          type: 'setup',
          data: {
            name: $settings.username,
            room: $settings.room
          }
        });
      };
      wss.onclose = e => {
        console.log(
          'WebSocket has been closed. Reconnection will be attempted in 3 seconds.',
          e.reason
        );
        setTimeout(connect, 3000);
      };

      wss.onmessage = packet => {
        let msg = JSON.parse(packet.data);
        console.log(msg);
        switch (msg.type) {
          case 'id':
            /**
             * {
             *  type: 'id',
             *  data: {
             *    uid: Number
             *   }
             * }
             */
            uid = msg.data.uid;
            break;
          case 'join':
            /**
             * {
             *  type: 'join',
             *  data: {
             *    name: String,
             *    uid: Number
             *   }
             * }
             */
            break;
          case 'leave':
            /**
             * {
             *  type: 'leave',
             *  data: {
             *    name: String,
             *    uid: Number
             *   }
             * }
             */
            break;
        }
      };

      wss.onerror = err => {
        if (err.message) console.error('WebSocket error: ', err.message);
        wss.close();
      };
    }

    setup();
  }
};
