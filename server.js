/**
 * heyyy - Say hiii, say heyyy, say g'day!
 * ========================================
 *
 * ISC License
 *
 * Copyright (c) 2019, dragonwocky <thedragonring.bod@gmail.com> (https://dragonwocky.me/)
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

/**
 * TODO:
 * - ?notifications: if enabled, a ding sound will be played to notify of new msgs
 * - store msgs in db
 *    - show time next to msgs: stored as UTC to be localised by client
 *    - ?history: if enabled, show messages sent in room previous to current load (localStorage)
 * - online/offline list (self diff colour), leave room option
 *    sent when the tab is unfocused (localStorage)
 * - mobile pwa, browser extension
 * - users
 *    - post editing
 *    - avatars
 *    - bots
 *
 * MARKDOWN (to be implemented):
 *  > = blockquote
 *  ` = code
 *  -- = overline
 *  ~~ = strikethrough
 *  __ = underline
 *  ** = bold
 *  * and _ = italics
 *  ![]() = image (served from backend to prevent unwanted communication between client and external)
 *  []() = link
 *  []{} = colour
 *
 * IMPLEMENTED:
 * - ?username: the user's name, will be displayed with an id number to
 *    differentiate between multiple - required, no spaces (localStorage)
 * - ?room: room to join, allows for semi-private chats or just main
 *    chatroom - no spaces (localStorage)
 * - ?autojoin: if values are valid bypass setup page
 */

const http = require('http'),
  url = require('url'),
  path = require('path'),
  fs = require('fs'),
  ws = require('ws'),
  uuidv4 = require('uuid/v4'),
  less = require('less');

const _ = {};

_.domain = process.env.PROJECT_DOMAIN
  ? `${process.env.PROJECT_DOMAIN}.glitch.me`
  : 'localhost';
_.port = process.env.PORT || 3000;

const server = http
  .createServer((req, res) => {
    switch (true) {
      case req.url === '/' + (url.parse(req.url).search || ''):
        fs.readFile(path.join('public', 'index.html'), 'utf8', (err, data) => {
          res.writeHead(200, { 'Content-Type': 'text/html' });
          res.end(data, 'binary');
        });
        break;
      case req.url === '/autosize.min.js':
        fs.readFile(
          path.join('public', 'autosize.min.js'),
          'utf8',
          (err, data) => {
            res.writeHead(200, { 'Content-Type': 'text/javascript' });
            res.end(data, 'binary');
          }
        );
        break;
      case req.url === '/scripts.js':
        fs.readFile(path.join('public', 'scripts.js'), 'utf8', (err, data) => {
          res.writeHead(200, { 'Content-Type': 'text/javascript' });
          res.end(data, 'binary');
        });
        break;
      case req.url === '/styles.css':
        fs.readFile(path.join('public', 'styles.less'), 'utf8', (err, data) => {
          less
            .render(data)
            .then(output => {
              res.writeHead(200, { 'Content-Type': 'text/css' });
              res.end(output.css, 'binary');
            })
            .catch(e => {
              console.log(e);
            });
        });
        break;
      case req.url === '/wave.png':
        fs.readFile(path.join('public', 'wave.png'), 'binary', (err, data) => {
          res.writeHead(200, { 'Content-Type': 'image/png' });
          res.end(data, 'binary');
        });
        break;
      case req.url === '/lumber.jpg':
        fs.readFile(
          path.join('public', 'lumber.jpg'),
          'binary',
          (err, data) => {
            res.writeHead(200, { 'Content-Type': 'image/jpg' });
            res.end(data, 'binary');
          }
        );
        break;
      case req.url === '/ding.mp3':
        fs.readFile(path.join('public', 'ding.mp3'), 'binary', (err, data) => {
          res.writeHead(200, { 'Content-Type': 'audio/mpeg' });
          res.end(data, 'binary');
        });
        break;
      default:
        res.writeHead(302, {
          Location: '/' + (url.parse(req.url).search || '')
        });
        res.end();
    }
  })
  .listen(_.port);
console.log(`Server running at http://${_.domain}:${_.port}/`);

const wss = new ws.Server({ server: server });

_.broadcast = (data, room) => {
  wss.clients.forEach(client => {
    if (client.readyState === ws.OPEN) {
      if (!room || client.room === room)
        client.send(JSON.stringify(data), error => {
          if (error) console.log(error);
        });
    }
  });
};

_.users = 0;

wss.on('connection', (socket, req) => {
  socket.parse = content => socket.send(JSON.stringify(content));
  socket.on('message', msg => {
    msg = JSON.parse(msg);
    console.log(msg);
    switch (msg.type) {
      case 'setup':
        /**
         * {
         *  type: 'setup',
         *  data: {
         *    name: String,
         *    room: String
         *   }
         * }
         */
        if (!msg.data.name) break;
        socket._name = msg.data.name;
        socket._room = msg.data.room;
        socket._uid = _.users++;
        socket.parse({ type: 'id', data: { uid: socket._uid } });
        _.broadcast(
          {
            type: 'join',
            data: {
              name: socket._name,
              uid: socket._uid
            }
          },
          socket._room
        );
        break;

      case 'message':
        /**
         * {
         *  type: 'message',
         *  data: {
         *    text: String,
         *    time: Date
         *   }
         * }
         */
        if (!msg.data.text || !msg.data.time) break;
        _.broadcast(
          {
            type: 'message',
            data: {
              name: socket._name,
              uid: socket._uid,
              text: msg.data.text,
              time: new Date(msg.data.time).toUTCString(),
              msgID: uuidv4()
            }
          },
          socket._room
        );
    }
  });

  socket.on('close', () => {
    if (socket._name && socket._uid && socket._room)
      _.broadcast(
        {
          type: 'leave',
          data: {
            name: socket._name,
            uid: socket._uid
          }
        },
        socket._room
      );
  });
});

// keep awake (for glitch.com)
setInterval(() => {
  http.get(`http://${_.domain}:${_.port}/`);
}, 150000);
